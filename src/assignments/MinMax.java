package assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MinMax {

	public static void main(String[] args) {
		
		int min = 0, max = 0, input;
	
		ArrayList<Integer> numbers = new ArrayList<>();
		ArrayList<Integer> minmax = new ArrayList<>();
		
		do {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter numbers: ");
			input = sc.nextInt();
			numbers.add(input);
		} while (numbers.size() < 5);
		
		System.out.println(numbers);
		Collections.sort(numbers);
		System.out.println(numbers);
		
		for(int i=0; i<numbers.size()-1; i++) {
			min += numbers.get(i);
		}
		
		for (int i=1; i<numbers.size(); i++) {
			max += numbers.get(i);
		}
		
		minmax.add(min);
		minmax.add(max);
		
		System.out.println(minmax);
		
		
		
	}

}
