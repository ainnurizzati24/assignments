package assignments;

import java.util.ArrayList;
import java.util.Scanner;

public class HighestPlacement {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int cse, ece, mech;
		boolean valid = false;
		
		System.out.println("Enter the no of students placed in CSE:");
		cse = sc.nextInt();
		
		System.out.println("Enter the no of students placed in ECE:");
		ece = sc.nextInt();
		
		System.out.println("Enter the no of students placed in MECH:");
		mech = sc.nextInt();
		
		valid = (cse >= 0 && ece >= 0 && mech >= 0) ? true : false;
		
		if(valid) {
			ArrayList <Integer> test = new ArrayList<Integer>();
			
			test.add(0, cse);
			test.add(1, ece);
			test.add(2, mech);
			
			if ((cse == ece) && (cse == mech)){
				System.out.println("None of the department has got the highest placement");
			} else if (cse > ece && cse > mech){
				System.out.println("Highest Placement");
				System.out.println("CSE");
			} else if (ece > cse && ece > mech){
				System.out.println("Highest Placement");
				System.out.println("ECE");
			} else if (mech > cse && mech > ece){
				System.out.println("Highest Placement");
				System.out.println("MECH");
			} else if (cse == ece){
				System.out.println("Highest Placement");
				System.out.println("CSE");
				System.out.println("ECE");
			} else if (cse == mech){
				System.out.println("Highest Placement");
				System.out.println("CSE");
				System.out.println("MECH");
			} else if (ece == mech){
				System.out.println("Highest Placement");
				System.out.println("ECE");
				System.out.println("MECH");
			} 
			
		} else {
			System.out.println("Input is invalid");
		}
		
		
		
	}
}
