package assignments;

import java.text.DecimalFormat;
import java.util.Scanner;

public class PetrolConsumption {
	
	private static void calculatePetrol(String vehicle, int money) {
		
		double litreCost = 115;
		double perLitre = 0;
		
		DecimalFormat dec = new DecimalFormat("0.00");

		if (vehicle.equals("car")) {
			perLitre = 8;
		} else  {
			perLitre = 20;
		} 
		
		double total =  money / litreCost;
		double distance =  total * perLitre;
		double miles =  distance/1.6;
		
		System.out.println("Total litre: " + dec.format(total) + " litre");
		System.out.println("Total distance in km: " +  dec.format(distance) + "km");
		System.out.println("Total distance in miles: " +  dec.format(miles) + "miles");

	}
	
	public static void main(String[] args) {
		
		int money;
		String vehicle;

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter your vehicle (Car/Bike): ");
		vehicle = sc.nextLine().toLowerCase();
		
		if (vehicle.equals("car")) {
			vehicle = "car";
		} else if (vehicle.equals("bike")) {
			vehicle="bike";
		} else {
			System.out.println("Please enter valid vehicle");
			return;
		}
		
		System.out.println("Enter amount of money (Rs): ");
		money = sc.nextInt();

		calculatePetrol(vehicle, money);
	}

}
