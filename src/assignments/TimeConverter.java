package assignments;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeConverter {

	public static void main(String[] args) throws ParseException {
		
		String input;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter 12 hour format time: ");
		input = sc.nextLine();
		
		DateFormat inFormat = new SimpleDateFormat("hhmmaa");
		DateFormat outFormat = new SimpleDateFormat("HHmm");
		
		Date date=null;
		date=inFormat.parse(input);
		
		if (date!=null){
			String myDate = outFormat.format(date);
			System.out.println(myDate);
		}
		
		sc.close();
		
	}

}
