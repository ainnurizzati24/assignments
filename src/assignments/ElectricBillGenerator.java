package assignments;

import java.util.Scanner;

public class ElectricBillGenerator {

	public static int calculateBill(int unit, int age) {
		
		int total = 0;
		int perUnit = 0;
		int penalty = 0;
		
			if ((unit >= 101) && (unit <= 200)){
				perUnit = 10;
			} 
			else if ((unit >= 201) && (unit <= 400)){
				perUnit = 25;
			} 
			else if ((unit >= 401) && (unit <= 1000)){
				perUnit = 50;
			} 
			else if (unit > 1000){
				penalty = 500000;
				System.out.println("Your line will be disconnected and penalty charge will be applied.");
				return penalty;
			} else {
				System.out.println("Your bill is free.");
				total = 0;
			}
			
			total = perUnit*unit;
			
			if (age > 60 && age <= 80) {
				total*=0.5;
				System.out.println("Your bill has 50% discount");
			} else if (age > 80 && age <= 100) {
				total*=0.2;
				System.out.println("Your bill has 80% discount");
			} else if (age > 100) {
				total = 0;
				System.out.println("Your bill is free");
			} else {
				return total;
			}
				
		return total;
		
	}
	
	public static void main(String[] args) {
		
		int unit, age;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("Enter no. of units: ");
			unit = sc.nextInt();
			System.out.println("Enter your age: ");
			age = sc.nextInt();
		} while (unit < 0 || age < 0);
		
		sc.close();

		int result =  calculateBill(unit, age);
		
		System.out.println("Total amount: " + result + "Rs");
		
	}

}
