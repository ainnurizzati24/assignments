package assignments;

import java.util.Scanner;

public class AverageEmployee {

	private static void calculateAverage (int[] age) {
		
		int sum = 0;
		int avg = 0;
		
		for(int i=0; i<age.length; i++) {
			sum+=age[i];
		}
			
		avg = sum/age.length;

		System.out.println("The average is " + avg);

	}
	
	public static void main(String[] args) {

		int number;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter total no. of employees: ");
		number = sc.nextInt();
		
		int[] age = new int[number];
		
		if(number < 2) {
			System.out.println("Please enter a valid employee count");
		} else {
			System.out.println("Enter the age for " + number + " employees: ");
			
			for(int i=0; i<number; i++) {
				age[i] =sc.nextInt();
				
				if ((age[i] < 28) || (age[i] >40)) {
					System.out.println("Invalid age encountered!");
					return;
				} else {
					sc.nextLine();
					
				}
			}
			calculateAverage(age);
		}		
		 
	}

}
