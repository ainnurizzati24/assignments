package assignments;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Theater {

	public static void calculateTotal(int ticket, String refresh, String coupon, String circle) {
		
		DecimalFormat decfor = new DecimalFormat("0.00");  
		
		double total = 0;
		int price = 0;
		int addOn = 0;
		double discount10 = 0;
		double discount2 = 0;
		
		discount10 = ticket > 20 ? 0.9 : 1;
		
		addOn = refresh.equals("y") ? 50 : 0;
		
		discount2 = coupon.equals("y") ? 0.98 : 1;
		
		price = circle.equals("k") ? 75 : 150;
		
		total = (ticket * price) * discount10 * discount2 + (ticket * addOn);
		System.out.println("Total: " +(total));
		
		return;
		
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int ticket = 0;
		String refresh= " ";
		String coupon = " ";
		String circle = " ";

		System.out.println("Enter the no of tickets:");
		ticket = sc.nextInt();
		sc.nextLine();
		
		if (ticket >= 5 && ticket <=40) {
			System.out.println("Do you want refreshment (y/n): ");
			refresh = sc.nextLine();

			if (refresh.equalsIgnoreCase("y")|| refresh.equalsIgnoreCase("n")){
				
				System.out.println("Do you have coupon code (y/n):");
				coupon = sc.nextLine();
				
				if (coupon.equalsIgnoreCase("y")|| coupon.equalsIgnoreCase("n")){
					System.out.println("Enter the circle (k/q): ");
					circle = sc.nextLine();
					
					if (circle.equalsIgnoreCase("k")|| circle.equalsIgnoreCase("q")){
						
						calculateTotal(ticket, refresh, coupon, circle);
						
					} else {
						System.out.println("Invalid input");
					}
					
				} else {
					System.out.println("Invalid input");
				}
			} else {
				System.out.println("Invalid input");
			}
			
		} else {
			System.out.println("Minimum of 5 and Maximum of 40 Tickets");
		}
			
		sc.close();
	}

}
